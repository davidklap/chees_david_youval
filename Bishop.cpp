#include "Bishop.h"
#include "Board.h"
#include "empty.h" 


Bishop::Bishop(int row, char col, kind_of_piece kind, Color_related_to_the_player related)
	: piece(row, col, kind, related)
{
}

Bishop::~Bishop()
{
}

std::vector<Point> Bishop::opecnele_index_to_moov(Point &start_index, Board &board)
{
	
	std::vector<Point> v;
	Point p(0,0);

	int start_row = start_index.getRow();
	int start_col = start_index.getCol() - 'a';
	Color_related_to_the_player color = board.board[start_row][start_col]->_related;
	int row;
	int col =0;
	// we need olso to check that ira not a shah
	// first we start with the mov  too the up right
	for (row = start_row,col = start_col ; (row -1 >=0 && col +1 < COL_SIZE) && board.board[row-1][col+1]->_related != color; row--,col++)
	{
		p.setCol(col + 'a'+1);
		p.setRow(row-1);

		v.push_back(p);
		if (board.board[row - 1][col + 1]->_related != Transparent)// its men that he can eat a pice of the enmy there and he need to stop there 
		{
			break;
		}
	}

	

	// the mov up left


	for (row = start_row, col = start_col; (row  -1 >= 0 && col -1 >=0) && board.board[row-1][col-1]->_related != color; row--,col--)
	{
		p.setCol(col + 'a'-1);
		p.setRow(row-1);

		v.push_back(p);
		if (board.board[row - 1][col - 1]->_related != Transparent)// its men that he can eat a pice of the enmy there and he need to stop there 
		{
			break;
		}
	}

	// mov down right
	for (row = start_row, col = start_col; (row + 1  < ROW_SIZE && col +1 < COL_SIZE ) && board.board[row+1][col+1]->_related != color; row++,col++)
	{
		p.setCol(col + 'a'+1);
		p.setRow(row+1);

		v.push_back(p);
		if (board.board[row + 1][col + 1]->_related != Transparent)// its men that he can eat a pice of the enmy there and he need to stop there 
		{
			break;
		}
	}

	// down left
	for (row = start_row, col = start_col; (row + 1 < ROW_SIZE  && col - 1 >= 0 ) && board.board[row+1][col-1]->_related != color; row++ ,col--)
	{
		p.setCol(col + 'a'-1);
		p.setRow(row+1);

		v.push_back(p);
		if (board.board[row + 1][col - 1]->_related != Transparent)// its men that he can eat a pice of the enmy there and he need to stop there 
		{
			break;
		}
	}

	
	return v;
}

void Bishop::mov(Point &destion, Board &board)
{

	// gating the  src loction of  ghe bishopp
	int src_row = _loction.getRow();
	int src_col = _loction.getCol() - 'a';
	src_row = ROW_SIZE - src_row;
	//src_col = COL_SIZE - src_col;
	// mov to the  destion place in the borad the bishop
	board.board[destion.getRow()][destion.getCol() - 'a'] = board.board[src_row][src_col];
	board.board[src_row][src_col] = new Empty(src_row, src_col, emptys, Transparent); // seting the src place pf the bishop to empty place

	// set thhe new pice in the destion loctoion loction
	board.board[destion.getRow()][destion.getCol() - 'a']->_loction.setRow(ROW_SIZE - destion.getRow());
	board.board[destion.getRow()][destion.getCol() - 'a']->_loction.setCol(destion.getCol());
}
