#pragma once
#include <vector>
#include <map>
#include "Point.h"
// enum that define the kind of the piecce
typedef enum kind_of_piece {
	king,
	queen,
	rooks,
	bishops,
	knights,
	pawns,
	emptys
}kind_of_piece;
// RELATED THE PICE TO THE COLOR 
typedef enum Color_related_to_the_player {
	black,
	white,
	Transparent // clear, sakouf 
}Color_related_to_the_player;


typedef enum player_color {
	black_c,
	white_c
}player_color;

typedef enum LoctionFromGrafics {
	src_col,
	src_row,
	dest_col,
	dest_row
}LoctionFromGrafics;
/*
typedef enum code_for_grafic111 {
	legel_mov,
	legel_and_ches,
	not_legel_enemi_src,
	not_legel_dest_taken_buy_u,
	not_legel_wiil_make_a_chess,
	not_legel_invalied_index,
	not_legel_invalid_mov,
	not_legel_same_src_and_dest,
	legel_chess_mat
}code_for_grafic111;

char const* eror_to_graffic[] = 
{
	"0",
	"1",
	"2",
	"3",
	"4",
	"5",
	"6",
	"7",
	"8"
}
*/
