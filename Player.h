#pragma once
#include <iostream>
#include "generalEnums.h"
class Player
{
public:
	Player(Color_related_to_the_player color);
	~Player();
	bool chess_mat();
	Color_related_to_the_player getColor();
private:
	Color_related_to_the_player _color;

};

