#include "Point.h"

Point::Point(int row, char col)
{
	setCol(col);
	setRow(row);
}

Point::~Point()
{
}

void Point::setRow(int row)
{
	_row = row;
}

void Point::setCol(char col)
{
	_col = col;
}

int Point::getRow() const
{
	return _row;
}

char Point::getCol() const
{
	return _col;
}

bool Point::operator==(Point other)
{
	// chek if its the sine place 
	if (this->getCol() == other.getCol() && this->getRow() == other.getRow())
	{
		return true;
	}
	// else 
	return false;
}
		
