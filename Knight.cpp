#include "Knight.h"
#include "empty.h"
#include "Board.h"
Knight::Knight(int row, char col, kind_of_piece kind, Color_related_to_the_player related)
	: piece(row, col, kind, related)
{
}

Knight::~Knight()
{
}

std::vector<Point> Knight::opecnele_index_to_moov(Point &start_index, Board &board)
{

	// importentt to sy thatt the knight wlaking 3:2 to ech  way exapmle 3 up 2 left/right
	int row = start_index.getRow();
	int col = start_index.getCol() - 'a';
	Color_related_to_the_player color = board.board[row][col]->_related; // the colr of the knight 
	Point p(0,'a');
	std::vector<Point> v;
	// up  right 
	if ((row - 2 >= 0 && col + 1 < COL_SIZE) // CHECK IF INDEX IS NOT OUT OF BOUNS
		&& board.board[row - 2][col + 1]->_related != color) // chck that tthis place is not taken buy onther of his pices
	{
		p.setCol(col + 1 + 'a');
		p.setRow(row - 2);
		v.push_back(p);
	}
	// up  left
	if ((row - 2 >= 0 &&  col -1 >= 0) // CHECK IF INDEX IS NOT OUT OF BOUNS
		&& board.board[row - 2][col - 1]->_related != color) // chck that tthis place is not taken buy onther of his pices
	{
		p.setCol(col - 1 + 'a');
		p.setRow(row - 2);
		v.push_back(p);
	}
	//right up 
	if ((row - 1 >= 0  && col + 2 < COL_SIZE) // CHECK IF INDEX IS NOT OUT OF BOUNS
		&& board.board[row - 1][col + 2]->_related != color) // chck that tthis place is not taken buy onther of his pices
	{
		p.setCol(col + 2 + 'a');
		p.setRow(row - 1);
		v.push_back(p);
	}

	//riht down
	if ((row + 1 < ROW_SIZE && col + 2 < COL_SIZE) // CHECK IF INDEX IS NOT OUT OF BOUNS
		&& board.board[row +1][col + 2]->_related != color) // chck that tthis place is not taken buy onther of his pices
	{
		p.setCol(col + 2 + 'a');
		p.setRow(row + 1);
		v.push_back(p);
	}
	// down rightt
	if ((row + 2 < ROW_SIZE  && col + 1 < COL_SIZE) // CHECK IF INDEX IS NOT OUT OF BOUNS
		&& board.board[row + 2][col + 1]->_related != color) // chck that tthis place is not taken buy onther of his pices
	{
		p.setCol(col + 1 + 'a');
		p.setRow(row + 2);
		v.push_back(p);
	}

	// down left 
	if ((row + 2  < ROW_SIZE && col - 1 >= 0) // CHECK IF INDEX IS NOT OUT OF BOUNS
		&& board.board[row + 2][col - 1]->_related != color) // chck that tthis place is not taken buy onther of his pices
	{
		p.setCol(col - 1 + 'a');
		p.setRow(row + 2);
		v.push_back(p);
	}

	// left up
	if ((row - 1 >= 0 && col - 2 >=0) // CHECK IF INDEX IS NOT OUT OF BOUNS
		&& board.board[row - 1][col - 2]->_related != color) // chck that tthis place is not taken buy onther of his pices
	{
		p.setCol(col - 2 + 'a');
		p.setRow(row - 1);
		v.push_back(p);
	}

	// left down 
	if ((row + 1 < ROW_SIZE && col - 2 >= 0) // CHECK IF INDEX IS NOT OUT OF BOUNS
		&& board.board[row + 1][col - 2]->_related != color) // chck that tthis place is not taken buy onther of his pices
	{
		p.setCol(col  - 2 + 'a');
		p.setRow(row + 1);
		v.push_back(p);
	}

	return v;
}

void Knight::mov(Point &destion, Board &board)
{
	
	// gating the  src loction of  ghe bishopp
	int src_row = _loction.getRow() ;
	int src_col = _loction.getCol() - 'a';
	src_row = ROW_SIZE - src_row;
	//src_col = COL_SIZE - src_col;
	// mov to the  destion place in the borad the bishop
	board.board[destion.getRow()][destion.getCol() - 'a'] = board.board[src_row][src_col];
	board.board[src_row][src_col] =  new Empty(src_row, src_col, emptys, Transparent); // seting the src place pf the bishop to empty place

	// set thhe new pice in the destion loctoion loction
	board.board[destion.getRow()][destion.getCol() - 'a']->_loction.setRow(ROW_SIZE- destion.getRow());
	board.board[destion.getRow()][destion.getCol() - 'a']->_loction.setCol(destion.getCol());
	

}
