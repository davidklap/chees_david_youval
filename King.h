#pragma once
#include "piece.h"
#include "generalEnums.h"

class board;
class King : public piece
{
public:
	King(int row, char col, kind_of_piece kind, Color_related_to_the_player related);
	~King();
	std::vector<Point> opecnele_index_to_moov(Point &start_index, Board &board);
	void mov(Point &destion, Board &board);
private:

};

