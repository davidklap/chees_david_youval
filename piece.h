#pragma once
#include "Point.h"
#include <vector>
#include "generalEnums.h"
#include <iostream>
class Board;


class piece
{
public:
	piece(int row, char col, kind_of_piece kind, Color_related_to_the_player related);
	~piece();
	virtual std::vector<Point> opecnele_index_to_moov(Point &start_index, Board &board) = 0;
	virtual void mov(Point &destion, Board &board) = 0;
	Point _loction;
	kind_of_piece _kind;
	Color_related_to_the_player _related;


};
