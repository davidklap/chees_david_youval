#pragma once
class Point
{
public:
	Point(int row, char col);
	~Point();
	void setRow(int row);
	void setCol(char col);
	int getRow() const;
	char getCol() const;
	bool operator == (Point other);
private:
	int _row;
	char _col;
};
