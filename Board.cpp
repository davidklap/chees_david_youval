#include "Board.h"
#include "Bishop.h"
#include "empty.h"
#include "King.h"	
#include "Knight.h"
#include "Pawn.h"
#include "Queen.h"
#include "Rook.h"

Board::Board() :
	_kingLoction_b(0, 'd'),
	_kingLoction_w(7, 'd')
{
	// in start we are diclarete the board for base situision 


	// black base setionon 

	board[0][0] = new Rook(8, 'a', rooks, black);
	board[0][1] = new Knight(8, 'b', knights, black);
	board[0][2] = new Bishop(8, 'c', bishops, black);
	board[0][3] = new King(8, 'd', king, black);
	board[0][4] = new Queen(8, 'e', queen, black);
	board[0][5] = new Bishop(8, 'f', bishops, black);
	board[0][6] = new Knight(8, 'g', knights, black);
	board[0][7] = new Rook(8, 'h', rooks, black);
	board[1][0] = new Pawn(7, 'a', pawns, black);
	board[1][1] = new Pawn(7, 'b', pawns, black);
	board[1][2] = new Pawn(7, 'c', pawns, black);
	board[1][3] = new Pawn(7, 'd', pawns, black);
	board[1][4] = new Pawn(7, 'e', pawns, black);
	board[1][5] = new Pawn(7, 'f', pawns, black);
	board[1][6] = new Pawn(7, 'g', pawns, black);
	board[1][7] = new Pawn(7, 'h', pawns, black);

	// Empty board base

	board[2][0] = new Empty(6, 'a', emptys, Transparent);
	board[2][1] = new Empty(6, 'b', emptys, Transparent);
	board[2][2] = new Empty(6, 'c', emptys, Transparent);
	board[2][3] = new Empty(6, 'd', emptys, Transparent);
	board[2][4] = new Empty(6, 'e', emptys, Transparent);
	board[2][5] = new Empty(6, 'f', emptys, Transparent);
	board[2][6] = new Empty(6, 'g', emptys, Transparent);
	board[2][7] = new Empty(6, 'h', emptys, Transparent);
	board[3][0] = new Empty(6, 'a', emptys, Transparent);
	board[3][1] = new Empty(5, 'b', emptys, Transparent);
	board[3][2] = new Empty(5, 'c', emptys, Transparent);
	board[3][3] = new Empty(5, 'd', emptys, Transparent);
	board[3][4] = new Empty(5, 'e', emptys, Transparent);
	board[3][5] = new Empty(5, 'f', emptys, Transparent);
	board[3][6] = new Empty(5, 'g', emptys, Transparent);
	board[3][7] = new Empty(5, 'h', emptys, Transparent);
	board[4][0] = new Empty(4, 'a', emptys, Transparent);
	board[4][1] = new Empty(4, 'b', emptys, Transparent);
	board[4][2] = new Empty(4, 'c', emptys, Transparent);
	board[4][3] = new Empty(4, 'd', emptys, Transparent);
	board[4][4] = new Empty(4, 'e', emptys, Transparent);
	board[4][5] = new Empty(4, 'f', emptys, Transparent);
	board[4][6] = new Empty(4, 'g', emptys, Transparent);
	board[4][7] = new Empty(4, 'h', emptys, Transparent);
	board[5][0] = new Empty(3, 'a', emptys, Transparent);
	board[5][1] = new Empty(3, 'b', emptys, Transparent);
	board[5][2] = new Empty(3, 'c', emptys, Transparent);
	board[5][3] = new Empty(3, 'd', emptys, Transparent);
	board[5][4] = new Empty(3, 'e', emptys, Transparent);
	board[5][5] = new Empty(3, 'f', emptys, Transparent);
	board[5][6] = new Empty(3, 'g', emptys, Transparent);
	board[5][7] = new Empty(3, 'h', emptys, Transparent);


	// white base setionon 

	board[6][0] = new Pawn(2, 'a', pawns, white);
	board[6][1] = new Pawn(2, 'b', pawns, white);
	board[6][2] = new Pawn(2, 'c', pawns, white);
	board[6][3] = new Pawn(2, 'd', pawns, white);
	board[6][4] = new Pawn(2, 'e', pawns, white);
	board[6][5] = new Pawn(2, 'f', pawns, white);
	board[6][6] = new Pawn(2, 'g', pawns, white);
	board[6][7] = new Pawn(2, 'h', pawns, white);
	board[7][0] = new Rook(1, 'a', rooks, white);
	board[7][1] = new Knight(1, 'b', knights, white);
	board[7][2] = new Bishop(1, 'c', bishops, white);
	board[7][3] = new King(1, 'd', king, white);
	board[7][4] = new Queen(1, 'e', queen, white);
	board[7][5] = new Bishop(1, 'f', bishops, white);
	board[7][6] = new Knight(1, 'g', knights, white);
	board[7][7] = new Rook(1, 'h', rooks, white);


}

Board::~Board()
{
}

/*

fonction create a string that show the currnt stioinion of the board
input: null
output : the string
*/
std::string Board::create_String_Of_board()
{

	
	std::string board_str;
	int i = 0;
	int j = 0;

	for (i = 0; i < ROW_SIZE; i++)
	{
		for (j = 0; j < COL_SIZE; j++)
		{
			switch (board[i][j]->_kind)
			{

			case rooks:
				if (board[i][j]->_related == black)
				{
					board_str.append("r");
					break;
				}
				if (board[i][j]->_related == white)
				{
					board_str.append("R");
					break;
				}
	
			case knights:
				if (board[i][j]->_related == black)
				{
					board_str.append("n");
					break;
				}
				if (board[i][j]->_related == white)
				{
					board_str.append("N");
					break;
				}
			
			case bishops:
				if (board[i][j]->_related == black)
				{
					board_str.append("b");
					break;
				}
				if (board[i][j]->_related == white)
				{
					board_str.append("B");
					break;
				}
			
			case king:
				if (board[i][j]->_related == black)
				{
					board_str.append("k");
					break;
				}
				if (board[i][j]->_related == white)
				{
					board_str.append("K");
					break;
				}
			
			case queen:
				if (board[i][j]->_related == black)
				{
					board_str.append("q");
					break;
				}
				if (board[i][j]->_related == white)
				{
					board_str.append("Q");
					break;
				}
			
			case pawns:
				if (board[i][j]->_related == black)
				{
					board_str.append("p");
					break;
				}
				if (board[i][j]->_related == white)
				{
					board_str.append("P");
					break;
				}
			case  emptys :
				board_str.append("#"); // becuse thr the place dont relete to any one 
				break;
			default:
				break;
			}
		}
	}
	board_str.append("0");
	return board_str;

}

bool Board::opcinale_chees(Point &king_loction, Board &b) const
{
	Color_related_to_the_player color_to_check = board[king_loction.getRow()][king_loction.getCol() - 'a']->_related;
	std::vector<Point> v;
	int row = 0;
	int col = 0;
	Point p(0,'a');
	for (; row < ROW_SIZE ; row++)
	{
		for (col = 0; col < COL_SIZE; col++)
		{
			if (board[row][col]->_related != color_to_check && board[row][col]->_related != Transparent)
			{
				p.setCol(col + 'a');
				p.setRow(row);
				v = board[row][col]->opecnele_index_to_moov(p, b);
				for (Point current_point_vecor : v)
				{
					if (current_point_vecor == king_loction)
						return true;
				}
			}
		}
	}
	return false;
}
