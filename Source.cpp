/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project,
in order to read and write information from and to the Backend
*/

#include "Pipe.h"
#include <iostream>
#include <thread>
#include "generalEnums.h"
#include "Board.h"
#include "Bishop.h"
#include "empty.h"
#include "King.h"
#include "Knight.h"
#include "Pawn.h"
#include "Queen.h"
#include "Rook.h"
#include "useGrafic.h"
#include "Player.h"
using std::cout;
using std::endl;
using std::string;


void main()
{
	srand(time_t(NULL));
	Board chess_board;
	Point point(0, 'a');
	Point src(0, 'a');
	Point dest(0, 'a');
	std::vector<Point> v; // wiil conctine the vector of the opcionele movment  
	string ans;
	Pipe p;
	Player current_player(white);
	Player white_player(white);
	Player black_player(black);
	bool isConnect = p.connect();
	bool chess_mat = false; // it wiil include the reruens valio o the foncttion that check if ttis a chess mett 
	char msgToGraphics[1024]; // wiil conttaine the strof the borad that we send to grafic 
	bool is_One_Of_The_Pice_opconel_indedx = false;
	piece *temp = new Empty(0,'a',emptys,Transparent);
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else
		{
			p.close();
			return;
		}
	}
	

	strcpy_s(msgToGraphics, chess_board.create_String_Of_board().c_str()); //copy the strin of the borad 
	p.sendMessageToGraphics(msgToGraphics);   // send the board string

	// get message from graphics

	string msgFromGraphics; 
	while (msgFromGraphics != "quit")
	{
		// should handle the string the sent from graphics
		// according the protocol. Ex: e2e4           (move e2 to e4)
		// the gmame 
			is_One_Of_The_Pice_opconel_indedx = false;

			// get the loction from tthe facking grafics 
			msgFromGraphics = p.getMessageFromGraphics();
		 	getLoctionFromGrafic(msgFromGraphics, src, dest);

			v = chess_board.board[src.getRow()][src.getCol() - 'a']->opecnele_index_to_moov(src, chess_board); // get the opcenele ondex that the pice can go
			// check if the src place is legel 
			if (chess_board.board[src.getRow()][src.getCol() - 'a']->_related != current_player.getColor())
			{
				strcpy_s(msgToGraphics, "2");
				p.sendMessageToGraphics(msgToGraphics);
				continue;
			}
			// check if the dest loction include one of your piece
			if (chess_board.board[dest.getRow()][dest.getCol() - 'a']->_related == current_player.getColor())
			{
				strcpy_s(msgToGraphics, "3");
				p.sendMessageToGraphics(msgToGraphics);
				continue;

			}
			// if the src and the dest is eulw but impirtet to say that its not realy posibole becuse the grefic not aloud it 
			if (src == dest)
			{
				strcpy_s(msgToGraphics, "7");
				p.sendMessageToGraphics(msgToGraphics);
				continue;
			}
			//check if the place is one of the opcinele places of the pices 
			size_t   i = 0;
			for (;i < v.size();i++)
			{
				if (v[i] == dest)
					is_One_Of_The_Pice_opconel_indedx = true;
			}
			// if its not onclude its an aror 
			if (!is_One_Of_The_Pice_opconel_indedx)
			{
				strcpy_s(msgToGraphics, "6");
				p.sendMessageToGraphics(msgToGraphics);
				continue;
			}
			// we keping the dest for case that he cant nov tp ther becus of a chhess
			temp = chess_board.board[dest.getRow()][dest.getCol() - 'a'];
			chess_board.board[src.getRow()][src.getCol() - 'a']->mov(dest, chess_board);
			// check the king of the player for knoe wich clor tto check 
			if (current_player.getColor() == white)
			{
				if (chess_board.opcinale_chees(chess_board._kingLoction_w, chess_board))
				{
					// returns the borad to the  original sitonion
					chess_board.board[src.getRow()][src.getCol() - 'a'] = chess_board.board[dest.getRow()][dest.getCol() - 'a']; // set tthe src to the to the original place 
					chess_board.board[dest.getRow()][dest.getCol() - 'a'] = temp; // set the dest
					strcpy_s(msgToGraphics, "4");
					p.sendMessageToGraphics(msgToGraphics);
					continue;
				}
			}
			
			if (current_player.getColor() == black)
			{
				if (chess_board.opcinale_chees(chess_board._kingLoction_b, chess_board))
				{
					// returns the borad to the  original sitonion
					chess_board.board[src.getRow()][src.getCol() - 'a'] = chess_board.board[dest.getRow()][dest.getCol() - 'a']; // set tthe src to the to the original place 
					chess_board.board[dest.getRow()][dest.getCol() - 'a'] = temp; // set the dest
					strcpy_s(msgToGraphics, "4");
					p.sendMessageToGraphics(msgToGraphics);
					continue;
				}
			}

			// if its do a chees on the enmy 
			if (current_player.getColor() == white)
			{
				if (chess_board.opcinale_chees(chess_board._kingLoction_b, chess_board))
				{
					strcpy_s(msgToGraphics, "1");
					p.sendMessageToGraphics(msgToGraphics);
					if (current_player.getColor() == black)
					{
						current_player = white_player;
					}
					else
					{
						current_player = black_player;
					}
					continue;
				}
			}
			if (current_player.getColor() == black)
			{
				if (chess_board.opcinale_chees(chess_board._kingLoction_w, chess_board))
				{
					strcpy_s(msgToGraphics, "1");
					p.sendMessageToGraphics(msgToGraphics);
					// becuse we dont want to send 0 to the grafic butt wee stiil want to switch a player 
					if (current_player.getColor() == black)
					{
						current_player = white_player;
					}
					else
					{
						current_player = black_player;
					}
					continue;
				}
			}
			// its alrefy mov the piece
			strcpy_s(msgToGraphics, "0");
			p.sendMessageToGraphics(msgToGraphics);


			
			if (current_player.getColor() == black)
			{
				current_player = white_player;
			}
			else
			{
				current_player = black_player;
			}
		countin:;
		
		
		/******* JUST FOR EREZ DEBUGGING ******/
		int r = rand() % 10; // just for debugging......
		msgToGraphics[0] = (char)(1 + '0');
		msgToGraphics[1] = 0;
		/******* JUST FOR EREZ DEBUGGING ******/
	}

	p.close();
}

