#pragma once
#include "piece.h"
#include "Board.h"
class board;
class Pawn : public piece
{
public:
	Pawn(int row, char col, kind_of_piece kind, Color_related_to_the_player related);
	~Pawn();
	std::vector<Point> opecnele_index_to_moov(Point &start_index, Board &board);
	void mov(Point &destion, Board &board);
private:
};
