#pragma once
#include "piece.h"
#include "Board.h"
class board;
class Rook : public piece
{
public:
	Rook(int row, char col, kind_of_piece kind, Color_related_to_the_player related);
	~Rook();
	std::vector<Point> opecnele_index_to_moov(Point &start_index, Board &board);
	void mov(Point &destion, Board &board);

private:

};
