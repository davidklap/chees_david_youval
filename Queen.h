#pragma once
#include "piece.h"
#include "Board.h"

class board;
class Queen : public piece
{
public:
	Queen(int row, char col, kind_of_piece kind, Color_related_to_the_player related);
	~Queen();
	std::vector<Point> opecnele_index_to_moov(Point &start_index, Board &board);
	void mov(Point &destion, Board &board);

};

